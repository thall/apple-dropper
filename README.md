# Apple Dropper

Apple dropper, a clone of [Kaboom](https://archive.org/details/a8b_Kaboom_1983_Activision_US_a_k_file) for the Atari made in unity with C#.
Control the baskets with your mouse and keep the apples from hitting the ground.
Run out of your baskets by missing apples, and the game is over. Go for the high score, you know the drill.

## Demo Clip:

[Video Here](appledropper.mp4)

## Additional Features:

- gnome


Copyright 2022 Tessa Hall

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
