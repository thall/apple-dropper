using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeSpawner : MonoBehaviour
{
		public float spawnDelay = 5;
		public float spawnRate = 2;
		public GameObject apple;
		public float speed;
		public GameObject tree;
		private Vector3 cVect = Vector3.right;
		
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Spawn", spawnDelay, spawnRate);
    }

		// roomba mode
		void Flip() {
				if (cVect == Vector3.right) cVect = Vector3.left;
				else cVect = Vector3.right;
		}

    // Update is called once per frame
		void Update() {
				float cX = transform.position.x;
				if ((-7 > cX) || (cX > 7)) Flip();
				transform.Translate((cVect * Time.deltaTime)*4, Space.World);
		}
		
    void Spawn()
    {
				Instantiate (apple, transform.position, Quaternion.identity);
    }
}
