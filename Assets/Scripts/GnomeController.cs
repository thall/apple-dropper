﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GnomeController : MonoBehaviour
{
		public float moveSpeed;
    private float hMovement;

    // Update is called once per frame
    void FixedUpdate()
    {
        ApplyInput();
    }

    void ApplyInput()
    {
				Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				Vector2 flatVect = new Vector2(mousePosition.x, transform.position.y);
				transform.position = Vector2.MoveTowards(transform.position, flatVect, moveSpeed * Time.deltaTime);
    }
}
