using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class basketDestroy : MonoBehaviour
{
		public GameObject[] baskets = null;
		public int maxBasketIndex = 2;
		AudioSource audioData;
    // Start is called before the first frame update
    void Start()
    {
				baskets = GameObject.FindGameObjectsWithTag("basket");
    }

		void OnCollisionEnter2D(Collision2D other)
    {
				Debug.Log("test");
				if (maxBasketIndex == 0) {
						Time.timeScale = 0;
						audioData = GetComponent<AudioSource>();
						audioData.Play(0);
				}
				else
				{
						Destroy(baskets[maxBasketIndex]);
						maxBasketIndex -= 1;
				}
    }
}
